#!/bin/bash

set -e

skip_modules=0
cmdline_file=""
target_host=""
img_file=""
def_cmdline=""
partition=4
out_dir="."
make_args=""     # Extra (potentially) arch specific make args
vbutil_args=""
vmlinuz=""
rootdev=""
rootdev_extra="" # for some devices we need to add a "p", ie /dev/mmcblk1p3

# Base path for finding other helper scripts:
base=`dirname $0`

function usage
{
	echo "Usage:"
	echo "$0 [ -s ] [ -c <cmdline_file> ] [ -o <path> ] -h <target_host>"
	echo "    -s update stable (KERN_A) partition"
	echo "    -c <cmdline_file> file containing kernel command line"
	echo "       [default fetched from device]"
	echo "    -o <path> corresponds to O=<path> argument for kernel build"
	echo "    -h <target_host> target machine to update"
	echo ""
	echo "    Updates <target_host> with the kernel and modules from"
	echo "    the current directory.  Defaults to installing the kernel"
	echo "    into the KERN_B partition and marking it as 'boot once'"
	exit 1
}

function error
{
	echo "ERROR: $1"
	exit 1
}

# helper to execute commands on target device via ssh:
function on_dut
{
	ssh root@$target_host $*
}

function default_cmdline
{
	def_cmdline=`mktemp /tmp/push_kernel_cmdline-XXXX`
	# Fetch cmdline from target device:
	on_dut cat /proc/cmdline | sed 's/ /\n/g' > $def_cmdline
}

function copy_modules
{
	dir=`mktemp -d /tmp/push_kernel-XXXX`
	echo -n "installing modules..."
	make $make_args INSTALL_MOD_PATH=$dir INSTALL_MOD_STRIP=1 modules_install >& /dev/null
	echo "done."
	echo -n "copying modules..."
	on_dut mount -o remount,rw /
	(cd $dir/lib/modules; rsync -a * root@$target_host:/lib/modules/)
	if [ $? -ne 0 ] ; then
		on_dut mount -o remount,ro /
		return -1
	fi
	echo "done."
	on_dut mount -o remount,ro /
	rm -rf $dir
	return 0
}

function build_image
{
	img_file=`mktemp /tmp/push_kernel_img-XXXX`

	# Fetch signing files from target:
	scp root@$target_host:/usr/share/vboot/devkeys/kernel.keyblock $base/kernel.keyblock
	scp root@$target_host:/usr/share/vboot/devkeys/kernel_data_key.vbprivk $base/kernel_data_key.vbprivk

	vbutil_kernel $vbutil_args \
		      --pack $img_file \
		      --keyblock $base/kernel.keyblock \
		      --signprivate $base/kernel_data_key.vbprivk \
		      --version 1 \
		      --config "$cmdline_file" \
		      --vmlinuz $vmlinuz \
		      --arch $target_arch
	if [ ! -f $img_file ]; then
		return -1
	fi
	if [ "$def_cmdline" != "" ]; then
		rm $def_cmdline
	fi
	return $?
}
	
function copy_image
{
	# Setup new kernel to boot from partition B
	echo -n "copying kernel image..."
	scp $img_file root@$target_host:/tmp >& /dev/null
	if [ $? -ne 0 ] ; then
		echo "failed to copy kernel image"
		exit 0
	fi
	rm $img_file
	echo "done."
	return 0
}

function install_image
{
	echo -n "installing kernel to partition $partition..."
	on_dut dd if=$img_file of=$rootdev$rootdev_extra$partition bs=4K >& /dev/null
	if [ $? -ne 0 ] ; then
		echo "failed to write kernel image on target"
		exit 0
	fi
	echo "done."
}

function set_boot_once
{
	if [ $partition -eq 2 ] ; then
		echo -n "setting boot flags on KERN_A..."
		on_dut cgpt add -i 2 -S 1 -T 1 -P14 $rootdev
	else
		echo -n "setting boot once flag for KERN_B..."
		on_dut cgpt add -i 4 -S 0 -T 1 -P15 $rootdev
	fi
	if [ $? -ne 0 ] ; then
		echo "failed to set new kernel image active on target"
		exit 0
	fi
	echo "done."
}

optstr="sc:h:o:"

while getopts $optstr opt; do
	case $opt in
		's')
			partition=2
			echo "updating stable kernel (KERN_A)"
			;;
		'c')
			cmdline_file=$OPTARG
			echo "using $cmdline_file for boot command line"
			;;
		'h')
			target_host=$OPTARG
			echo "sending kernel to $target_host"
			;;
		'o')
			make_args="$make_args O=$OPTARG"
			echo "adding make arg: O=$OPTARG"
			out_dir=$OPTARG
			;;
		*)
			usage
			;;
	esac
done

if [ "$target_host" == "" ]; then
	usage
fi

if [ "$cmdline_file" == "" ]; then
	default_cmdline
	cmdline_file=$def_cmdline
	echo "using default command line"
fi

# Find where the partitions live:
rootdev=`on_dut rootdev | sed 's/[0-9]*$//'`
if [ "$rootdev" != "${rootdev%p}" ]; then
	rootdev_extra="p"
	rootdev=${rootdev%p}
fi
echo "Found root device: $rootdev"

# Figure out the target arch:
target_arch="`on_dut uname -m`"
echo "target arch is: $target_arch"

# Target arch specific setup/config:
if [ "$target_arch" == "aarch64" ]; then
	make_args="$make_args ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-"

	# On aarch64 we can use a dummy bootloader.bin:
	bl=`mktemp /tmp/push_kernel_bootloader_bin-XXXX`
	dd if=/dev/zero of=$bl bs=512 count=1
	vbutil_args="--bootloader $bl"

	# Generate fitImage
	vmlinuz=`mktemp /tmp/push_kernel_Image_fit-XXXX`
	fit_compression="lz4"    # TODO get this programatically?
	soc_vendor="qcom"        # TODO get this programatically?
	$base/generate-its-script.sh -a arm64 -c $fit_compression \
			$out_dir/arch/arm64/boot/Image \
			$out_dir/arch/arm64/boot/dts/$soc_vendor/*.dtb | \
		dtc -I dts -O dtb -p 1024 > $vmlinuz
elif [ "$target_arch" == "x86_64" ]; then
	vbutil_args="--bootloader $base/bootstub.efi"
	vmlinuz="$out_dir/arch/x86/boot/bzImage"
fi

if [ ! -f $vmlinuz ] ; then
	echo "run from a linux tree with the kernel built"
	exit 1
fi

if [ ! -x "$(command -v vbutil_kernel)" ]; then
	echo "vbutil_kernel not installed, install vboot-kernel-utils"
	exit 1
fi

copy_modules
if [ $? -ne 0 ]; then
	echo "module copy failed"
	exit 1
fi

build_image
if [ $? -ne 0 ]; then
	echo "image build failed"
	exit 1
fi

copy_image
if [ $? -ne 0 ]; then
	echo "image copy failed"
	exit 1
fi

install_image
if [ $? -ne 0 ]; then
	echo "image install failed"
	exit 1
fi

set_boot_once
if [ $? -ne 0 ]; then
	echo "failed to set kernel to boot"
	exit 1
fi

echo "success, $target_host is ready to reboot."
exit 0

