#!/bin/bash
#
# Script to fetch the current kernel config from the target
# device.  Default ChromeBook kernels have CONFIG_IKCONFIG=m
# and CONFIG_IKCONFIG_PROC=y, so we need to `modprobe configs`
# to read out the config

target_host=""
output_config=".config"

function usage() {
	echo "Usage:"
	echo "$0 [ -c <config> ] -h <target_host>"
	echo "    -c <config> - output config, default .config"
	echo "    -h <target_host> - target machine to get config from"
	exit 1
}

optstr="c:h:"

while getopts $optstr opt; do
	case $opt in
		'c')
			output_config=$OPTARG
			echo "writing config to $output_config"
			;;
		'h')
			target_host=$OPTARG
			echo "fetching config from $target_host"
			;;
		*)
			usage
			;;
	esac
done

if [ "$target_host" == "" ]; then
	usage
fi

on_dut() {
	ssh $target_host $*
}

on_dut modprobe configs
on_dut zcat /proc/config.gz > $output_config
