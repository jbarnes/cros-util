﻿Upstream Development on Chromebooks
===================================

This is a collection of utility scripts and docs to make it easier for
folks to use chromebooks for upstream Linux development, without requiring
a complete [ChromeOS Developement Environment](https://chromium.googlesource.com/chromiumos/docs/+/main/developer_guide.md).

Upstream kernels on ChromeOS
----------------------------

You may want to run a stock upstream kernel on a Chromebook running
ChromeOS for a variety of reasons:

* Testing and development of upstream patches
* Pushing upstream patches
* Doing upstream development
* Doing distro development on a Chromebook

To that end, I think it’s nice to be able to work in a regular,
upstream git kernel tree and be able to easily push your kernel
over to a target device for development and testing.  Further,
using the stock Chromebook firmware and boot method (vboot) is
desirable for reasons of test coverage, consistency of experience,
and general Chromeyness.


The high level requirements for this are similar to many other
development activities:

* Chromebook in [developer mode](https://chromium.googlesource.com/chromiumos/docs/+/master/developer_mode.md)
* ssh access to the target device
* Workstation with kernel sources
* vboot-kernel-utils on the workstation (Depending on host distro
  the package name may instead be "vboot-utils")

Additionally, if USB boot is not enabled (doesn't show up in
the firmware boot menu once developer mode is enabled), it
is strongly recommended to enable it by running `enable_dev_usb_boot`
on the target device.  Booting from a known good image on a
USB drive is the most straightforward way to recover after
flashing a kernel that doesn't boot.

On a high level, the steps required are the following:

1. Build your kernel with an appropriate config and necessary
   patches (see appendix)
2. Copy modules to the target device
3. Build the new kernel image, including command line, signing,
   and boot stub
4. Install the new image
5. Set the new image to boot once


The script in the appendix will perform these steps for you and
is provided as a reference.


Appendix A - command line and config
------------------------------------

A sample command line is included in the repo:

```
loglevel=7
init=/sbin/init
root=/dev/nvme0n1p3
rootwait
rw
i915.modeset=1
```

It will depend on your specific platform (some will have root
on `/dev/mmc*` for example, or not use the i915 graphics driver.


The kernel configuration to use will depend on the platform as
well, but options like `CONFIG_VT` and `CONFIG_FRAMEBUFFER`
should be disabled if booting Chrome OS. See this example config
for reference.


See example hatch config in this dir.


On Chrome OS, the current config can also be retrieved using
`modprobe configs` and then reading `/proc/config.gz`.


Appendix B - Scripts
--------------------

```
git clone https://gitlab.freedesktop.org/jbarnes/cros-util.git
```

Use the `fetch_config.sh` script to fetch the devices current
kernel config.  From there, you can `make olddefconfig`.

Use the `push_kernel.sh` script to upload a kernel from your current
working directory (which should be a kernel tree) to the target system.
The target has to be in developer mode with ssh access, and you’ll
also need to provide a kernel boot command line.  You can base your
command line on `/proc/cmdline` from the running system.


Appendix C - SuzyQ
------------------

[SuzyQ](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/master/docs/ccd.md#SuzyQ-SuzyQable)
is a USB debug cable that provides (among other things) serial port
access to:

* Cr50 console
* CPU/AP console (ie. where the linux serial console is)
* EC console

The USB-C end of the SuzyQ plugs into the chromebook.  If the device
has two USB-C ports, only one of the two will have CCD/SuzyQ support.
And orientation of the USB-C connector matters.  With the right
combination of USB-C port and connector orientation, you should see
three new `/dev/ttyUSB*` ports, which you can connect to with screen,
kermit, etc.  The middle one should be the CPU/AP console.


Appendix D - Recovering From Bad Kernel
---------------------------------------

The most straightforward way to recover if you get the chromebook
into a non-booting state is to boot a known good image from a USB
drive.  From there you can run `chromeos-install` to completely
wipe and re-flash the internal storage (the `--preserve_stateful`
argument will preserve your local user account).

Alternately, if you just want to update kernel you can use `dd`
top copy the kernel from the USB disk.  (Note, you may need to
mount rootfs and copy modules as well).  For example, the internal
storage device is `/dev/mmcblk1`, you can dump the partition
table and see the two boot partitions (A & B):

```
localhost ~ # fdisk -l /dev/mmcblk1
Disk /dev/mmcblk1: 58.2 GiB, 62537072640 bytes, 122142720 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 50D9FEEE-A2CD-174D-87D9-8E474D590F02

Device             Start       End   Sectors  Size Type
/dev/mmcblk1p1  17076224 122142671 105066448 50.1G Linux filesystem
/dev/mmcblk1p2        69     65604     65536   32M ChromeOS kernel
/dev/mmcblk1p3   8687616  17076223   8388608    4G ChromeOS root fs
/dev/mmcblk1p4     65605    131140     65536   32M ChromeOS kernel
/dev/mmcblk1p5    299008   8687615   8388608    4G ChromeOS root fs
/dev/mmcblk1p6        65        65         1  512B ChromeOS kernel
/dev/mmcblk1p7        66        66         1  512B ChromeOS root fs
/dev/mmcblk1p8    135168    167935     32768   16M Linux filesystem
/dev/mmcblk1p9        67        67         1  512B ChromeOS reserved
/dev/mmcblk1p10       68        68         1  512B ChromeOS reserved
/dev/mmcblk1p11       64        64         1  512B unknown
/dev/mmcblk1p12   167936    299007    131072   64M EFI System

Partition table entries are not in disk order.
```

And similarly for the USB device (which in this case is
`/dev/sda` and has the same layout):
```
localhost ~ # dd if=/dev/sda2 of=/dev/mmcblk1p2 
65536+0 records in
65536+0 records out
33554432 bytes (34 MB, 32 MiB) copied, 0.316393 s, 106 MB/s
localhost ~ # dd if=/dev/sda2 of=/dev/mmcblk1p4
65536+0 records in
65536+0 records out
33554432 bytes (34 MB, 32 MiB) copied, 0.367268 s, 91.4 MB/s
localhost ~ # 
localhost ~ # mount /dev/mmcblk1p3 /mnt
localhost ~ # cp -r /lib/modules/5.4.88-12156-ge831bd591b38 /mnt/lib/modules/
localhost ~ # umount /mnt
localhost ~ # mount /dev/mmcblk1p5 /mnt
localhost ~ # cp -r /lib/modules/5.4.88-12156-ge831bd591b38 /mnt/lib/modules/
localhost ~ # umount /mnt
```

Appendix E - Example Workflow
-----------------------------

(Starting from kernel src directory)

```
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5  ~/src/cros-util/fetch_config.sh -c aarch64/.config -h trogdor
writing config to aarch64/.config
fetching config from trogdor
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5  make O=aarch64 CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 olddefconfig
  HOSTCC  scripts/kconfig/conf.o
  HOSTCC  scripts/kconfig/confdata.o
  HOSTCC  scripts/kconfig/expr.o
  LEX     scripts/kconfig/lexer.lex.c
  YACC    scripts/kconfig/parser.tab.[ch]
  HOSTCC  scripts/kconfig/lexer.lex.o
  HOSTCC  scripts/kconfig/parser.tab.o
  HOSTCC  scripts/kconfig/preprocess.o
  HOSTCC  scripts/kconfig/symbol.o
  HOSTCC  scripts/kconfig/util.o
  HOSTLD  scripts/kconfig/conf
#
# using defaults found in /boot/config-5.10.8-200.fc33.x86_64
#
/boot/config-5.10.8-200.fc33.x86_64:734:warning: symbol value 'm' invalid for KVM
/boot/config-5.10.8-200.fc33.x86_64:2266:warning: symbol value 'm' invalid for MTD_NAND_ECC_SW_HAMMING
#
# configuration written to .config
#
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5 
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5  make -j8 O=aarch64 CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 modules Image.gz dtbs
  HOSTCC  scripts/dtc/dtc.o
  HOSTCC  scripts/dtc/flattree.o
  HOSTCC  scripts/dtc/fstree.o
...
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5 
 robclark  dellirium  ~/src/linux   tag:v5.11-rc5  ~/src/cros-util/push_kernel.sh -o aarch64 -h trogdori
adding make arg: O=aarch64
sending kernel to trogdor
using default command line
Found root device: /dev/mmcblk1
target arch is: aarch64
...
installing modules...done.
copying modules...done.
copying kernel image...done.
installing kernel to partition 4...done.
setting boot once flag for KERN_B...done.
success, trogdor is ready to reboot.

```
